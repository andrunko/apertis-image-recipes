{{ $architecture := or .architecture "amd64" }}
{{ $type := or .type "minimal" }}
{{ $mirror := or .mirror "https://repositories.apertis.org/apertis/" }}
{{ $suite := or .suite "18.06" }}
{{ $timestamp := or .timestamp "00000000.0" }}
{{ $ospack := or .ospack (printf "ospack_%s-%s-%s_%s.tar.gz" $suite $architecture $type $timestamp) }}
{{ $image := or .image (printf "apertis-%s-%s-%s_%s" $suite  $type $architecture $timestamp) }}

{{ $cmdline := or .cmdline "console=tty0 console=ttyS0,115200n8 rootwait ro quiet splash plymouth.ignore-serial-consoles" }}

architecture: {{ $architecture }}

actions:
  - action: unpack
    description: Unpack {{ $ospack }}
    compression: gz
    file: {{ $ospack }}

  - action: image-partition
    imagename: {{ $image }}.img
{{ if eq $type "minimal" }}
    imagesize: 7G
{{end}}
{{ if eq $type "target" "development" }}
    imagesize: 15G
{{end}}
    partitiontype: gpt

    mountpoints:
      - mountpoint: /
        partition: system
        flags: [ boot ]
        options: [ ro ]
      - mountpoint: /boot/efi
        partition: EFI
      - mountpoint: /home
        partition: general_storage

    partitions:
      - name: EFI
        fs: vfat
        start: 6176s
        end: 256M
        flags: [ boot ]
      - name: factory_reset
        fs: vfat
        start: 256M
        end: 511M
      - name: flags
        fs: none
        start: 511M
        end: 512M
      - name: boot_1
        fs: vfat
        start: 512M
        end: 544M
      - name: boot_2
        fs: vfat
        start: 544M
        end: 576M
      - name: system
        fs: btrfs
        start: 576M
{{ if eq $type "minimal" }}
        end: 4672M
{{end}}
{{ if eq $type "target" "development" }}
        end: 8672M
{{end}}
      - name: general_storage
        fs: btrfs
{{ if eq $type "minimal" }}
        start: 4672M
{{end}}
{{ if eq $type "target" "development" }}
        start: 8672M
{{end}}
        end: -4M
      - name: flags_backup
        fs: none
# Temporary due bug in parted with negative values:
# https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=880035
#       start: -4M
{{ if eq $type "minimal" }}
        start: 6996M
{{end}}
{{ if eq $type "target" "development" }}
        start: 14996M
{{end}}
        end: -6177s

  - action: filesystem-deploy
    description: Deploying ospack onto image

  - action: apt
    packages:
      - gummiboot

  - action: run
    description: Install UEFI bootloader
    chroot: true
    command: bootctl --path=/boot/efi install

  - action: run
    description: Add kernel parameters
    chroot: true
    command: sed -i -e 's%GUMMIBOOT_OPTIONS=.*%GUMMIBOOT_OPTIONS="{{ $cmdline }}"%' /etc/default/gummiboot

  - action: apt
    description: Kernel and system packages for {{$architecture}}
    packages:
      - linux-image-{{$architecture}}
      - btrfs-tools
      - libgles2-mesa

  - action: run
    description: Cleanup /var/lib
    script: scripts/remove_var_lib_parts.binary

  - action: run
    description: Create block map for {{ $image }}.img
    postprocess: true
    command: bmaptool create {{ $image }}.img > {{ $image }}.img.bmap

  - action: run
    description: Compress {{ $image }}.img
    postprocess: true
    command: gzip -f {{ $image }}.img

  - action: run
    description: Checksum for {{ $image }}.img.gz
    postprocess: true
    command: md5sum {{ $image }}.img.gz > {{ $image }}.img.gz.md5
